extends Area2D

class_name Item

var initial_position: Vector2
var initial_parent: Node
var is_picked := false

func _enter_tree() -> void:
	if initial_parent == null:
		initial_position = global_position
		initial_parent = get_parent()

func interact(player) -> bool:
	if is_picked:
		return false

	is_picked = true
	return player.pick_item(self)

func return_to_initial() -> void:
	get_parent().remove_child(self)
	initial_parent.add_child(self)
	global_position = initial_position
	is_picked = false
