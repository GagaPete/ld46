extends Area2D

const BLOOD = preload("res://game/floor/blood.png")
const BLOOD_RECT = Rect2(Vector2(), Vector2(5, 5))
const BLOOD_2 = preload("res://game/floor/blood2.png")
const BLOOD_2_RECT = Rect2(Vector2(), Vector2(5, 8))
const WIDTH = 1024

var blood_queue: Array
var image: Image

func _ready() -> void:
	blood_queue = Array()

	image = Image.new()
	image.create(WIDTH, 10, false, Image.FORMAT_RGBA8)
	var texture = ImageTexture.new()
	texture.create_from_image(image, 0)
	$Sprite.texture = texture

func _process(_delta: float) -> void:
	if blood_queue.empty():
		return

	for position in blood_queue:
		var graphic := BLOOD
		var rect := BLOOD_RECT
		if randf() < 0.05:
			graphic = BLOOD_2
			rect = BLOOD_2_RECT
		image.blend_rect(graphic, rect, position + Vector2(0, randi() % 2));

	var texture = ImageTexture.new()
	texture.create_from_image(image, 0)
	$Sprite.texture = texture
	
	blood_queue = Array()
	set_process(false)

func _on_Floor_area_entered(area: Area2D) -> void:
	if not area is BloodParticle:
		return

	var position = area.global_position - global_position - (BLOOD_RECT.size * 0.5)
	blood_queue.append(position)
	area.queue_free()
	set_process(true)
