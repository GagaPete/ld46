extends Area2D

class_name SpecialTree

const DEMAND_NOTE = preload("res://game/tree/DemandNote.tscn");

signal state_changed(name)

var next_state: TreeState
var state: TreeState
var lifetime := 0.0
var demand_note: DemandNote
var dirty := true

func _ready() -> void:
	state = $States/Idle
	state._tree = self
	state.enter()

func _process(delta: float) -> void:
	if next_state != null:
		state.leave()
		state = next_state
		next_state = null
		state._tree = self
		state.enter()
		emit_signal("state_changed", state.name)

	lifetime += delta
	state.process(delta)

func change_state(name:String) -> void:
	next_state = get_node("States/%s" % name)

func play_wiggle() -> void:
	var wiggle_animation := "wiggle" if dirty else "wiggle_silverless"
	$AnimatedSprite.play(wiggle_animation)
	$WiggleSound.play()
	yield($WiggleSound, "finished")
	if $AnimatedSprite.animation != wiggle_animation:
		# Don't return to idle if animation has changed in the meantime
		return
	$AnimatedSprite.play("idle" if dirty else "idle_silverless")
	$AnimatedSprite.stop()

func play_grow() -> void:
	$AnimatedSprite.play("grow" if dirty else "grow_silverless")

func play_die() -> void:
	$AnimatedSprite.play("die")

func play_take() -> void:
	$AnimatedSprite.play("take")

func clean() -> void:
	if not dirty:
		return

	dirty = false
	$AnimatedSprite.animation += "_silverless"

func set_demand(item: String) -> void:
	clear_demand()
	demand_note = DEMAND_NOTE.instance()
	demand_note.item = item
	add_child(demand_note)

func clear_demand() -> void:
	if demand_note != null:
		demand_note.remove()
		demand_note = null

func take() -> bool:
	if state.name != "Gone" and state.name != "Dead" and state.name != "Grown":
		change_state("Gone")
		return true
	return false

func interact(item) -> bool:
	return state.interact(item)

func get_phase() -> int:
	return int(floor(min(120.0, lifetime) / 40.0))
