extends Area2D

class_name Prince

const BLOOD_PARTICLE = preload("res://game/BloodParticle.tscn")

export var blood_count := 16
export var speed := 60.0

var direction := 0.0 setget set_direction

var state: PrinceState
var next_state: PrinceState

func _ready() -> void:
	state = $States/RunningForTheTree
	state._prince = self
	state.enter()

func _process(delta: float) -> void:
	# Detect direction on first update
	if direction == 0.0:
		set_direction(global_position.x * - 1)
	
	position.x += speed * direction * delta

	if next_state != null:
		state.leave()
		state = next_state
		next_state = null
		state._prince = self
		state.enter()

	state.process(delta)

func set_direction(new_direction: float) -> void:
	direction = sign(new_direction)
	scale.x = direction * -1

func show_stolen_tree() -> void:
	$StolenTree.visible = true

func change_state(name:String) -> void:
	next_state = get_node("States/%s" % name)

func _on_Prince_area_entered(area: Area2D) -> void:
	if not area is SpecialTree:
		return

	area.take()
	state.tree_contact(area)

func kill() -> void:
	var target = get_parent()
	for _i in range(0, blood_count):
		var particle = BLOOD_PARTICLE.instance()
		particle.velocity = Vector2(0, -40).rotated(PI * ((0.3 * randf()) - 0.15))
		target.call_deferred("add_child", particle)
		particle.global_position = global_position + Vector2(rand_range(-5, 5), rand_range(-41, 1))
	queue_free()

func game_ended():
	state.game_ended()
