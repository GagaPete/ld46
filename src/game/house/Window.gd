tool
extends Sprite

const LIGHTS_OFF = preload("res://game/house/window_off.png")
const LIGHTS_ON = preload("res://game/house/window_on.png")

export var lit := false setget set_lit

func _ready() -> void:
	update_texture()

func set_lit(new_lit: bool) -> void:
	lit = new_lit
	update_texture()

func update_texture() -> void:
	if lit:
		texture = LIGHTS_ON
	else:
		texture = LIGHTS_OFF
