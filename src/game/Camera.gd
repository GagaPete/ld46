extends Camera2D

var follow_player := true

func _process(_delta: float) -> void:
	if follow_player:
		global_position = $"../Player".global_position

func center() -> void:
	global_position = Vector2()
	drag_margin_left = 0.0
	drag_margin_right = 0.0
	follow_player = false
