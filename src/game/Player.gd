extends Node2D

class_name Player

export var speed := 30
export var friction := 0.7

onready var interactables := []
var velocity := 0.0
var item: Item
var interactive := true
var destination: float

func _enter_tree() -> void:
	TouchControls.connect("interact", self, "_on_touch_interact")

func _exit_tree() -> void:
	TouchControls.disconnect("interact", self, "_on_touch_interact")

func _process(delta: float) -> void:
	if interactive:
		var direction := 0.0
		direction += Input.get_action_strength("game_right") - Input.get_action_strength("game_left")
		direction += TouchControls.direction
		direction = clamp(direction, -1.0, 1.0)
		velocity += direction * speed * delta
		velocity *= friction
		velocity = min(velocity, speed)
	else:
		var diff = destination - global_position.x
		if diff < 0.0:
			velocity = max(diff, speed * delta * -1)
		else:
			velocity = min(diff, speed * delta)
	global_position.x = clamp(global_position.x + velocity, -480, 400)

	var animation := "stand"
	if abs(velocity) > 0.01:
		$InteractArea.scale.x = sign(velocity)
		$AnimatedSprite.flip_h = (velocity > 0)
		animation = "walk"

	if item == null:
		animation += "_arms_down"
	else:
		animation += "_arms_up"

	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)

func _input(event: InputEvent) -> void:
	if not interactive:
		return

	if event.is_pressed() and event.is_action("game_interact"):
		interact()

func _on_touch_interact() -> void:
	interact()

func interact() -> void:
	if interactables.size() > 0:
		for interactable in interactables:
			if interactable.interact(self):
				return
func return_to_tree() -> void:
	interactive = false
	return_item()
	destination = 30 * sign(global_position.x)

func _on_InteractArea_entered(area: Area2D) -> void:
	interactables.append(area)

func _on_InteractArea_exited(area: Area2D) -> void:
	interactables.erase(area)

func pick_item(new_item: Item) -> bool:
	if item != null:
		return_item()

	new_item.get_parent().remove_child(new_item)
	$ItemHook.add_child(new_item)
	new_item.position = Vector2()
	item = new_item
	return true

func return_item() -> void:
	if item == null:
		return

	item.return_to_initial()
	item = null
