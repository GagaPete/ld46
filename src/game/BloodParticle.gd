extends Area2D

class_name BloodParticle

var velocity := Vector2(0, 10)

func _process(delta: float) -> void:
	global_position += velocity * delta
	velocity += gravity_vec * gravity * delta
	if velocity.length() > 60:
		velocity = velocity.normalized() * 60
