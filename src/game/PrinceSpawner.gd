extends Node2D

const PRINCE = preload("res://game/Prince.tscn")

func stop():
	$Timer.stop()

func _on_Timer_timeout() -> void:
	var prince = PRINCE.instance()
	add_child(prince)
	prince.global_position = get_spawn_position()

func get_spawn_position():
	var spawn_points = $SpawnPoints.get_children()
	var chosen_spawn = randi() % spawn_points.size()
	return spawn_points[chosen_spawn].global_position;
