extends Sprite

const SHIRTS = [
	 preload("res://game/prince/randomization/shirts/0.png"),
	 preload("res://game/prince/randomization/shirts/1.png"),
	 preload("res://game/prince/randomization/shirts/2.png"),
	 preload("res://game/prince/randomization/shirts/3.png"),
	 preload("res://game/prince/randomization/shirts/4.png"),
	 preload("res://game/prince/randomization/shirts/5.png"),
	 preload("res://game/prince/randomization/shirts/6.png"),
	 preload("res://game/prince/randomization/shirts/7.png"),
];

func _ready() -> void:
	var index = (randi() % SHIRTS.size() + 1)
	if index == SHIRTS.size():
		visible = false
	else:
		texture = SHIRTS[index]
