extends Sprite

const CAPES = [
	preload("res://game/prince/randomization/capes/0.png"),
	preload("res://game/prince/randomization/capes/1.png"),
	preload("res://game/prince/randomization/capes/2.png"),
	preload("res://game/prince/randomization/capes/3.png"),
]

func _ready() -> void:
	var index = (randi() % CAPES.size() + 1)
	if index == CAPES.size():
		visible = false
	else:
		texture = CAPES[index]
