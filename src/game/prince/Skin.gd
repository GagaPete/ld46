extends Sprite

const SKINS = [
	 preload("res://game/prince/randomization/skins/0.png"),
	 preload("res://game/prince/randomization/skins/1.png"),
	 preload("res://game/prince/randomization/skins/2.png"),
	 preload("res://game/prince/randomization/skins/3.png"),
];

func _ready() -> void:
	var index = randi() % SKINS.size()
	texture = SKINS[index]
