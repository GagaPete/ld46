extends Sprite

const HAIRS = [
	 preload("res://game/prince/randomization/hairs/0.png"),
	 preload("res://game/prince/randomization/hairs/1.png"),
	 preload("res://game/prince/randomization/hairs/2.png"),
	 preload("res://game/prince/randomization/hairs/3.png"),
	 preload("res://game/prince/randomization/hairs/4.png"),
	 preload("res://game/prince/randomization/hairs/5.png"),
	 preload("res://game/prince/randomization/hairs/6.png"),
	 preload("res://game/prince/randomization/hairs/7.png"),
	 preload("res://game/prince/randomization/hairs/8.png"),
	 preload("res://game/prince/randomization/hairs/9.png"),
	 preload("res://game/prince/randomization/hairs/10.png"),
];

func _ready() -> void:
	var index = (randi() % HAIRS.size() + 1)
	if index == HAIRS.size():
		visible = false
	else:
		texture = HAIRS[index]
