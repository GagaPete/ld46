extends Node2D

const CAPES = [
	preload("res://game/prince/randomization/capes/0.png"),
	preload("res://game/prince/randomization/capes/1.png"),
	preload("res://game/prince/randomization/capes/2.png"),
	preload("res://game/prince/randomization/capes/3.png"),
]

const HAIRS = [
	preload("res://game/prince/randomization/hairs/0.png"),
	preload("res://game/prince/randomization/hairs/1.png"),
	preload("res://game/prince/randomization/hairs/2.png"),
	preload("res://game/prince/randomization/hairs/3.png"),
	preload("res://game/prince/randomization/hairs/4.png"),
	preload("res://game/prince/randomization/hairs/5.png"),
	preload("res://game/prince/randomization/hairs/6.png"),
	preload("res://game/prince/randomization/hairs/7.png"),
	preload("res://game/prince/randomization/hairs/8.png"),
	preload("res://game/prince/randomization/hairs/9.png"),
	preload("res://game/prince/randomization/hairs/10.png"),
]

const SHIRTS = [
	preload("res://game/prince/randomization/shirts/0.png"),
	preload("res://game/prince/randomization/shirts/1.png"),
	preload("res://game/prince/randomization/shirts/2.png"),
	preload("res://game/prince/randomization/shirts/3.png"),
	preload("res://game/prince/randomization/shirts/4.png"),
	preload("res://game/prince/randomization/shirts/5.png"),
	preload("res://game/prince/randomization/shirts/6.png"),
	preload("res://game/prince/randomization/shirts/7.png"),
]

const SHOES = [
	[
		preload("res://game/prince/randomization/shoes/0_0.png"),
		preload("res://game/prince/randomization/shoes/0_1.png"),
	],
	[
		preload("res://game/prince/randomization/shoes/1_0.png"),
		preload("res://game/prince/randomization/shoes/1_1.png"),
	],
	[
		preload("res://game/prince/randomization/shoes/2_0.png"),
		preload("res://game/prince/randomization/shoes/2_1.png"),
	],
	[
		preload("res://game/prince/randomization/shoes/3_0.png"),
		preload("res://game/prince/randomization/shoes/3_1.png"),
	],
]

const SKINS = [
	preload("res://game/prince/randomization/skins/0.png"),
	preload("res://game/prince/randomization/skins/1.png"),
	preload("res://game/prince/randomization/skins/2.png"),
	preload("res://game/prince/randomization/skins/3.png"),
]

const TROUSERS = [
	[
		preload("res://game/prince/randomization/trousers/0_0.png"),
		preload("res://game/prince/randomization/trousers/0_1.png"),
	],
	[
		preload("res://game/prince/randomization/trousers/1_0.png"),
		preload("res://game/prince/randomization/trousers/1_1.png"),
	],
	[
		preload("res://game/prince/randomization/trousers/2_0.png"),
		preload("res://game/prince/randomization/trousers/2_1.png"),
	],
	[
		preload("res://game/prince/randomization/trousers/3_0.png"),
		preload("res://game/prince/randomization/trousers/3_1.png"),
	],
	[
		preload("res://game/prince/randomization/trousers/4_0.png"),
		preload("res://game/prince/randomization/trousers/4_1.png"),
	],
	[
		preload("res://game/prince/randomization/trousers/5_0.png"),
		preload("res://game/prince/randomization/trousers/5_1.png"),
	],
]

func _draw() -> void:
	var image = Image.new()
	image.create(13, 42, false, Image.FORMAT_RGBA8)
	image.blend_rect(get_shirt(), Rect2(Vector2(), Vector2(7, 17)), Vector2(4, 13))
	image.blend_rect(get_skin(), Rect2(Vector2(), Vector2(7, 27)), Vector2(4, 2))
	image.lock()
	image.set_pixel(7, 5, Color.black)
	image.unlock()
	image.blend_rect(get_hair(), Rect2(Vector2(), Vector2(8, 16)), Vector2(5, 0))
	image.blend_rect(get_cape(), Rect2(Vector2(), Vector2(9, 10)), Vector2(5, 12))
	
	var shoe = get_shoe()
	var trouser = get_trouser()

	var frame1_image = Image.new()
	frame1_image.create(13, 42, false, Image.FORMAT_RGBA8)
	frame1_image.blend_rect(image, Rect2(0, 0, 13, 42), Vector2())
	frame1_image.blend_rect(trouser[0], Rect2(0, 0, 7, 10), Vector2(3, 29))
	frame1_image.blend_rect(shoe[0], Rect2(0, 0, 8, 3), Vector2(1, 39))
	
	var frame2_image = Image.new()
	frame2_image.create(13, 42, false, Image.FORMAT_RGBA8)
	frame2_image.blend_rect(image, Rect2(0, 0, 13, 42), Vector2())
	frame2_image.blend_rect(trouser[1], Rect2(0, 0, 8, 10), Vector2(2, 29))
	frame2_image.blend_rect(shoe[1], Rect2(0, 0, 7, 3), Vector2(2, 39))
	
	var frame1_texture = ImageTexture.new()
	frame1_texture.create_from_image(frame1_image, 0)
	$Frame1.texture = frame1_texture
	
	var frame2_texture = ImageTexture.new()
	frame2_texture.create_from_image(frame2_image, 0)
	$Frame2.texture = frame2_texture

func get_cape() -> Image:
	return CAPES[randi() % CAPES.size()]

func get_hair() -> Image:
	return HAIRS[randi() % HAIRS.size()]

func get_shirt() -> Image:
	return SHIRTS[randi() % SHIRTS.size()]

func get_shoe():
	return SHOES[randi() % SHOES.size()]

func get_skin() -> Image:
	return SKINS[randi() % SKINS.size()]

func get_trouser():
	return TROUSERS[randi() % TROUSERS.size()]

onready var frames = [
	$Frame1,
	$Frame2,
];
var active_frame = 0

func _on_Timer_timeout() -> void:
	frames[active_frame].visible = false
	active_frame = 1 - active_frame
	frames[active_frame].visible = true
