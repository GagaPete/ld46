extends Node

class_name PrinceState

var _prince

func enter() -> void:
	pass

func leave() -> void:
	pass

func process(_delta: float) -> void:
	pass

func tree_contact(tree: SpecialTree) -> void:
	pass

func game_ended() -> void:
	pass

func change_state(name: String) -> void:
	_prince.change_state(name)

func get_direction() -> float:
	return _prince.direction

func get_position() -> Vector2:
	return _prince.global_position

func turn_around() -> void:
	_prince.direction = _prince.direction * -1

func show_stolen_tree() -> void:
	_prince.show_stolen_tree()

func free_prince() -> void:
	_prince.queue_free()
