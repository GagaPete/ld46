extends PrinceState

func tree_contact(tree: SpecialTree) -> void:
	if not tree.take():
		return

	change_state("ReturningWithTree")

func game_ended() -> void:
	change_state("ReturningWithoutTree")
