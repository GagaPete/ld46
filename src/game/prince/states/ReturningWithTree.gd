extends PrinceState

func enter() -> void:
	turn_around()
	show_stolen_tree()

func process(delta: float) -> void:
	if abs(get_position().x) > 600:
		free_prince()
