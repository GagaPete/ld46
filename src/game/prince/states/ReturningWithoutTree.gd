extends PrinceState

func enter() -> void:
	turn_around()

func process(delta: float) -> void:
	if abs(get_position().x) > 600:
		free_prince()
