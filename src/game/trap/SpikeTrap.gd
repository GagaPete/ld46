tool
extends Area2D

export var open := false setget set_open
export var bloody := false setget set_bloody

func _enter_tree() -> void:
	update_sprite()

func set_bloody(new_bloody: bool) -> void:
	bloody = new_bloody
	update_sprite()

func set_open(new_open: bool) -> void:
	open = new_open
	update_sprite()

func interact(_item: Player) -> bool:
	if open:
		return false

	open = true
	update_sprite()
	return true

func _on_SpikeTrap_area_entered(area: Area2D) -> void:
	if not open:
		return

	if not area is Prince:
		return

	activate(area)

func activate(prince: Prince) -> void:
	prince.kill()
	open = false
	bloody = true
	update_sprite()

func update_sprite() -> void:
	var anim_sprite = get_node("AnimatedSprite")
	if anim_sprite == null:
		return

	var animation := "open" if open else "close"
	if bloody:
		animation += "_bloody"

	anim_sprite.play(animation)
