extends TreeState

export var required_item: String
export var time_til_death := 5.0

var countdown: float

func enter():
	countdown = time_til_death
	play_wiggle()
	set_demand(required_item)

func leave():
	clear_demand()

func process(delta: float) -> void:
	countdown -= delta
	if countdown <= 0.0:
		change_state("Dead")

func interact(player: Player) -> bool:
	if required_item == "":
		change_state("Idle")
		return true
	
	if player.item and player.item.name == required_item:
		if required_item == "Scissor":
			clean()
		player.return_item()
		change_state("Idle")
		return true

	return false
