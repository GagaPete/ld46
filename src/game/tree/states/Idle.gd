extends TreeState

export var lifetime_to_grow := 120.0
export var timeout := 5.0

var countdown: float

func enter() -> void:
	countdown = timeout

func process(delta: float) -> void:
	if get_lifetime() > lifetime_to_grow:
		change_state("Grown")

	countdown -= delta
	if countdown <= 0.0:
		change_to_demand()

func change_to_demand():
	var rand := randf()
	if rand <= 0.33:
		change_state("DemandWater")
	elif rand <= 0.66:
		change_state("DemandCut")
	else:
		change_state("DemandTalk")
