extends Node2D

class_name TreeState

var _tree

func enter() -> void:
	pass

func leave() -> void:
	pass

func change_state(name: String) -> void:
	_tree.change_state(name)

func process(_delta: float) -> void:
	pass

func interact(_player: Player) -> bool:
	return false

func get_lifetime() -> float:
	return _tree.lifetime

func play_wiggle() -> void:
	_tree.play_wiggle()

func play_grow() -> void:
	_tree.play_grow()

func play_die() -> void:
	_tree.play_die()

func play_take() -> void:
	_tree.play_take()

func clean() -> void:
	_tree.clean()

func set_demand(item: String) -> void:
	_tree.set_demand(item)

func clear_demand() -> void:
	_tree.clear_demand()
