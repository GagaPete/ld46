extends Sprite

class_name DemandNote

const ITEM_WATERING_CAN = preload("res://game/item/watering_can.png")
const ITEM_SCISSOR = preload("res://game/item/scissor.png")
const ITEM_INTERACT = preload("res://game/item/textbubble.png")

export var item: String setget set_item

func _ready() -> void:
	update_item()
	$AnimationPlayer.play("spawn")

func set_item(new_item: String) -> void:
	item = new_item
	update_item()

func update_item() -> void:
	match item:
		"WateringCan": $Item.texture = ITEM_WATERING_CAN
		"Scissor": $Item.texture = ITEM_SCISSOR
		"": $Item.texture = ITEM_INTERACT

func remove() -> void:
	$AnimationPlayer.play("remove")
	yield($AnimationPlayer, "animation_finished")
	queue_free()
