extends Node2D

signal interact

var direction := 0.0

var stick_index := -1
var stick_position := Vector2()

func _input(event: InputEvent) -> void:
	if event is InputEventScreenTouch:
		if stick_index == event.index:
			calculate_direction(event.position)
			if not event.is_pressed():
				stick_index = -1
				stick_position = Vector2()
				direction = 0.0
		elif stick_index == -1 and (event.position.x / get_viewport_rect().size.x) >= 0.5:
			stick_index = event.index
			stick_position = event.position
			calculate_direction(event.position)
		elif event.is_pressed():
			emit_signal("interact")

	if event is InputEventScreenDrag:
		if event.index == stick_index:
			calculate_direction(event.position)

func calculate_direction(position: Vector2) -> void:
	direction = (position.x - stick_position.x) / get_viewport_rect().size.x * 8.0
	direction = clamp(direction, -1.0, 1.0)
