extends Submenu

func _ready() -> void:
	if OS.has_feature("web"):
		$VBoxContainer/Quit.queue_free()

func _on_Play_pressed() -> void:
	get_tree().change_scene("res://Game.tscn")

func _on_HowToPlay_pressed() -> void:
	change_submenu("HowToPlay")

func _on_Fullscreen_toggled(button_pressed: bool) -> void:
	OS.window_fullscreen = button_pressed

func _on_Quit_pressed() -> void:
	get_tree().quit()
