extends Submenu

var current_page := 0

func enter() -> void:
	current_page = 0
	$HBoxContainer/NextPage.visible = true

func _on_Back_pressed() -> void:
	change_submenu("Main")

func _on_NextPage_pressed() -> void:
	var pages := $Pages.get_children()
	pages[current_page].visible = false
	current_page += 1
	if pages[current_page].name == "KeyControls" and OS.has_feature("mobile"):
		current_page += 1
	if pages[current_page].name == "TouchControls" and not OS.has_feature("mobile"):
		current_page += 1
	pages[current_page].visible = true
	if current_page == (pages.size() - 1):
		$HBoxContainer/NextPage.hide()
