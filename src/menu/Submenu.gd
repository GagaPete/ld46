extends Control

class_name Submenu

var _menu

func change_submenu(name: String) -> void:
	_menu.change_submenu(name)

func enter() -> void:
	pass
