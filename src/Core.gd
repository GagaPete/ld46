extends Node

func _input(event):
	if not event.is_pressed():
		return

	if event.is_action("core_fullscreen"):
		toggle_fullscreen()

	if event.is_action("core_screenshot"):
		take_screenshot()
	
	if event.is_action("core_quit"):
		quit()

func toggle_fullscreen():
	OS.window_fullscreen = not OS.window_fullscreen

func take_screenshot():
	var data = get_viewport().get_texture().get_data()
	data.flip_y()
	var filename = "user://screenshot_%d.png" % OS.get_unix_time()
	data.save_png(filename)
	print("Screenshot saved as %s" % filename)

func quit():
	get_tree().change_scene("res://Menu.tscn")
