extends Control

var active_submenu: Submenu

func _ready() -> void:
	active_submenu = $Main
	active_submenu._menu = self
	active_submenu.enter()

func change_submenu(name: String) -> void:
	var submenu = get_node(name)
	if submenu == null:
		print("Missing menu %s" % name)
		return
	
	submenu._menu = self
	active_submenu.visible = false
	active_submenu = submenu
	active_submenu.visible = true
	active_submenu.enter()
