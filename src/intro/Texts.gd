tool
extends Control

export var index := -1 setget set_index

onready var pages = $Pages.get_children()
onready var tween = $Tween

var old_page: Label
var new_page: Label

func _ready() -> void:
	for page in pages:
		page.visible = false
		page.modulate.a = 0.0

func _process(_delta: float) -> void:
	if not is_inside_tree():
		return

	set_process(false)
	tween.stop_all()
	old_page = new_page
	new_page = null
	if index >= 0 and index < pages.size():
		new_page = pages[index]

	if old_page != null:
		tween.interpolate_property(old_page, "modulate:a", old_page.modulate.a, 0.0, 0.4)
	elif new_page != null:
		new_page.visible = true
		tween.interpolate_property(new_page, "modulate:a", new_page.modulate.a, 1.0, 0.4)

func _on_tween_completed(object, key) -> void:
	if object == old_page:
		old_page.visible = false
		if new_page != null:
			new_page.visible = true
			tween.interpolate_property(new_page, "modulate:a", new_page.modulate.a, 1.0, 0.4)

func set_index(new_index: int) -> void:
	index = new_index
	set_process(true)
