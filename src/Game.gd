extends Node2D

func _ready() -> void:
	randomize()

func tree_grown() -> void:
	end_game()
	$End/CenterContainer/VBoxContainer/HappyFace.visible = true
	$End/CenterContainer/VBoxContainer/TryAgain.queue_free()
	yield(get_tree().create_timer(4.0), "timeout")
	$AnimationPlayer.play("happy_end")
	yield(get_tree().create_timer(4.0), "timeout")
	$End.visible = true

func tree_died() -> void:
	$End/CenterContainer/VBoxContainer/SadFace.visible = true
	end_game()
	yield(get_tree().create_timer(1.0), "timeout")
	$End.visible = true

func tree_stolen() -> void:
	$End/CenterContainer/VBoxContainer/SadFace.visible = true
	end_game()
	yield(get_tree().create_timer(1.5), "timeout")
	$End.visible = true

func end_game() -> void:
	$Camera.center()
	$PrinceSpawner.stop()
	$Player.return_to_tree()
	get_tree().call_group("princes", "game_ended")

func _on_Tree_state_changed(name) -> void:
	match name:
		'Grown': tree_grown()
		'Dead': tree_died()
		'Gone': tree_stolen()

func _on_TryAgain_pressed() -> void:
	get_tree().reload_current_scene()

func _on_Quit_pressed() -> void:
	get_tree().change_scene("res://Menu.tscn")
