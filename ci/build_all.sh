#!/bin/sh

sh ci/build_android.sh "Android"
sh ci/build_html5.sh "HTML5"
sh ci/build_macosx.sh "Mac OSX"
sh ci/build_archive.sh "Linux/X11" "Linux" "x86_64"
sh ci/build_archive.sh "Windows Desktop" "Windows" "exe"