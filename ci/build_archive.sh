#!/bin/sh

source "$(dirname "$0")/vars"
PRESET=$1
SUFFIX=$2
EXTENSION=$3

if [[ "" == "${PRESET}" || "" == "${SUFFIX}" || "" == "${EXTENSION}" ]]; then
    echo "Usage: ${0} PRESET SUFFIX EXTENSION"
    exit 1
fi

BASEDIR=$(realpath $(dirname "$0")/..)
ARCHIVE="${BASEDIR}/builds/${PROJECT}${SUFFIX}.zip"
WORKDIR=$(mktemp -d -t build-XXXXXXXX)

echo "Export ${PRESET} from ${BASEDIR} to ${ARCHIVE}"

godot --path "${BASEDIR}/src/" --export "${PRESET}" "${WORKDIR}/${PROJECT}.${EXTENSION}"
rm "${ARCHIVE}" -f
cd "${WORKDIR}"; zip "${ARCHIVE}" *
rm "${WORKDIR}" -rf