#!/bin/sh

source "$(dirname "$0")/vars"
PRESET=$1

if [[ "" == "${PRESET}" ]]; then
    echo "Usage: ${0} PRESET"
    exit 1
fi

BASEDIR=$(realpath $(dirname "$0")/..)
ARCHIVE="${BASEDIR}/builds/${PROJECT}HTML5.zip"
WORKDIR=$(mktemp -d -t build-XXXXXXXX)

echo "Export ${PRESET} from ${BASEDIR} to ${ARCHIVE}"

godot --path "${BASEDIR}/src/" --export "${PRESET}" "${WORKDIR}/${PROJECT}.html"
mv "${WORKDIR}/${PROJECT}.html" "${WORKDIR}/index.html"
rm "${ARCHIVE}" -f
cd "${WORKDIR}"; zip "${ARCHIVE}" *
rm "${WORKDIR}" -rf