#!/bin/sh

source "$(dirname "$0")/vars"
PRESET=$1

if [[ "" == "${PRESET}" ]]; then
    echo "Usage: ${0} PRESET"
    exit 1
fi

BASEDIR=$(realpath "$(dirname "$0")/..")
TARGET="${BASEDIR}/builds/${PROJECT}Android.apk"

echo "Export ${PRESET} from ${BASEDIR} to ${TARGET}"

godot --path "${BASEDIR}/src/" --export-debug "${PRESET}" "${TARGET}"