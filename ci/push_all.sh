#!/bin/sh

source "$(dirname "$0")/vars"

BASEDIR=$(realpath "$(dirname "$0")/..")

push () {
    SRC="$1"
    TARGET="$2"
    butler push "${SRC}" "${ITCHGAME}:${TARGET}"
}

push "${BASEDIR}/builds/${PROJECT}Android.apk" android
push "${BASEDIR}/builds/${PROJECT}HTML5.zip" html5
push "${BASEDIR}/builds/${PROJECT}Linux.zip" linux
push "${BASEDIR}/builds/${PROJECT}MacOSX.zip" macosx
push "${BASEDIR}/builds/${PROJECT}Windows.zip" windows